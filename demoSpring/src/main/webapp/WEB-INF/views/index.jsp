<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home</title>
 <link href="/resources/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body style = "padding-top: 50px; padding-left: 50px;">
			<div class="col-md-5">
				<form action="search" method="post" class="form-inline" >
				<div class="search" class= "input">
					<input type="text" name="searchInput" value="" class="form-control">
					<input type="submit"  value="SEARCH" class="btn btn-default">
				</div>
			</form>
			</div>
			<div style="float:left;">
				<a href="${pageContext.request.contextPath}/newBook" class = "btn btn-success">add</a>
			</div>
<div class = "col-md-8">
	<table class = "table table-striped">
	<thead>
		<tr>
		<th>name</th>
		<th>author</th>
		<th>description</th>
		<th>price</th>
		<th>edit</th>
		<th>delete</th>
		<th>view detail </th>
		<th>add to cart</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${list }" var="listBook">
			<tr>
			<td>${listBook.name }</td>
			<td>${listBook.author }</td>
			<td>${listBook.description }</td>
			<td>${listBook.price }</td>
			<td><a href="#">edit</a></td>
			<td><a href="${pageContext.request.contextPath}/delete?id=${listBook.id}">delete</a></td>
			<td><a href="${pageContext.request.contextPath}/single?id=${listBook.id }">view detail</a></td>
			<td><a href = "${pageContext.request.contextPath}/addToCart?id=${listBook.id }">add to cart</a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
</div>
</body>
</html>