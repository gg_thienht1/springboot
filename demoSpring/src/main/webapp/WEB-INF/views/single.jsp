<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>single</title>
 <link href="/resources/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body style="padding-top:50px; padding-left:50px;">
	<h1>view detail</h1>
	<div class="col-md-8">
		<table class = "table table-striped">
	<thead>
		<tr>
		<th>name</th>
		<th>author</th>
		<th>description</th>
		<th>price</th>
		<th>edit</th>
		</tr>
	</thead>
	<tbody>
			<tr>
			<td>${book.name }</td>
			<td>${book.author }</td>
			<td>${book.description }</td>
			<td>${book.price }</td>
			<td><a href="${pageContext.request.contextPath}/addToCart?id=${book.id }">add to cart</a></td>
			</tr>
	</tbody>
</table>
	</div>
</body>
</html>