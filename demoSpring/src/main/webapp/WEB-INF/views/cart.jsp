<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>cart</title>
 <link href="/resources/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body style="padding-top:50px; padding-left:50px;">
	<div class="col-md-8">
	<h2>view cart</h2>
		<table class = "table table-striped">
 	<thead>
 		<tr>
 			<th>name</th>
 			<th>description</th>
 			<th>quantity</th>
 			<th>price</th>
 		</tr>
 	</thead>
 	<tbody>
 		 <tr>
 		 <c:forEach items="${carts}" var="cart">
 		 	<tr><td><strong>${cart.bookEntity.name}</strong></td>
 		 	<td>${cart.bookEntity.description}</td>
 		 	<td>${cart.quantity }</td>
 		 	<td>${cart.bookEntity.price}</td></tr>
 		 </c:forEach>
 		 </tr>
 		 <tr><td colspan="4">Total price: ${totalprice} </td></tr>
 	</tbody>
 </table>
 	<a href = "/checkout" class="btn btn-danger">check out</a>
	</div>
</body>
</html>