<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>

<title>Insert title here</title>
 <link href="/resources/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body>
 <div class="col-md-5">
 	<form:form action="${action}" method="post" modelAttribute="book">
         <fieldset >
              <h2>Add a new book</h2>
               <div>
                   <label >Name (*)</label>
                 	 <form:input path="name" type="text" class="form-control" placeholder="Name" required="true"/>
                </div>
                <div>
                    <label >Author (*)</label>
                      <form:input path="author" type="text" class="form-control" placeholder="Author" required="true"/>
                 </div>
                   <div >
                      <label >price</label>
                      <form:input path="price" type="number" class="form-control" placeholder="price" required="true"/>
                    </div>
                    <div >
                        <label>description</label>
                        <form:textarea path="description" type="text" step="any" class="form-control" placeholder="write description for book" required="true"/>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-info">Save</button>
           </fieldset>
      </form:form>
 </div>
</body>
</html>