<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>checkout</title>
 <link href="/resources/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body>
	   <span><h2> please fill in form </h2></span>
            <form:form class="form-horizontal" action="/checkout" method="post" modelAttribute="order">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="email">Full name:</label>
                    <div class="col-sm-5">
                        <form:input  path="fullName" class="form-control" id="email" placeholder="Enter email"></form:input>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="pwd">Email:</label>
                    <div class="col-sm-5">
                        <form:input  class="form-control" path="email" id="pwd" placeholder="Enter email"></form:input>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="pwd">Address:</label>
                    <div class="col-sm-5">
                        <form:input  class="form-control" path="address" id="pwd" placeholder="Enter adress"></form:input>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="pwd">Phone Number:</label>
                    <div class="col-sm-5">
                        <form:input  class="form-control" path="phone" id="pwd" placeholder="Enter phone number"></form:input>
                    </div>
                </div>
                <div class="form-group text-center">
                    <div class="col-sm-offset-2 col-sm-5">
                        <button type="submit" class="btn btn-success">check out</button> &ensp;
                        <button type="reset" onclick="" class="btn btn-danger"><a href="/viewCart"> Cancel</a></button>
                    </div>

                </div>
            </form:form>
</body>
</html>