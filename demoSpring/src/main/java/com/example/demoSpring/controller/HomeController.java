package com.example.demoSpring.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import com.example.demoSpring.entity.BookEntity;
import com.example.demoSpring.repository.BookRepository;

@Controller
public class HomeController {
	@Autowired
	BookRepository bookRepository;
	
	@RequestMapping(value="/")
	public String Home(Model model) {
		List<BookEntity> book = (List<BookEntity>) bookRepository.findAll();
		model.addAttribute("list", book);
		return "index";
	}

    @RequestMapping(value = "/search", method = POST)
    public String search(@RequestParam("searchInput")String searchInput, Model model,HttpServletRequest request) {
        List<BookEntity> resultList;
        if (searchInput.isEmpty()) {
            resultList = (List<BookEntity>) bookRepository.findAll();
        } else {
            resultList = bookRepository.findByNameContaining(searchInput);
        }
        model.addAttribute("list", resultList);
  
        return "index";
    }


    @RequestMapping(value = "/delete", method = GET)
    public String deleteBook(BookEntity book) {
        bookRepository.delete(book);
        return "redirect:/";
    }

    
    @RequestMapping(value = "/single")
    public String showproductbyid(Model model, @RequestParam("id") int id) {
    	  BookEntity book= bookRepository.getOne(Integer.valueOf(id));
          model.addAttribute("book",book);
        return "single";
    }
    @RequestMapping(value = "/newBook", method = GET)
    public String showNewBook(Model model) {
        model.addAttribute("book", new BookEntity());
        model.addAttribute("action", "newBook");
        return "book";
    }

    @RequestMapping(value = "newBook", method = POST)
    public String saveBook(BookEntity book) {
        bookRepository.save(book);
        return "redirect:/";
    }
//    @RequestMapping(value = "/edit/{id}", method = GET)
//    public String showEditBook(Model model, @PathVariable int id) {
//        model.addAttribute("book", bookRepository.);
//        model.addAttribute("msg", "Update book information");
//        model.addAttribute("type", "update");
//        model.addAttribute("action", "/updateBook");
//        return "book";
//    }

    @RequestMapping(value = "/updateBook", method = POST)
    public String updateBook(@ModelAttribute BookEntity book) {
        bookRepository.save(book);
        return "redirect:/";
    }

//    @RequestMapping(value ="/single")
//    public String showBookById() {
//    	BookEntity book = bookRepository.findO
//    	return "single";
//    }
}
