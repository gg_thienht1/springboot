package com.example.demoSpring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demoSpring.entity.OrderDetailEntity;
@Repository
public interface OrderDetailRepository extends CrudRepository<OrderDetailEntity,Integer> {

}
