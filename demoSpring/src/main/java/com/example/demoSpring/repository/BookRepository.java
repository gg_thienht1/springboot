package com.example.demoSpring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demoSpring.entity.BookEntity;

@Repository
public interface BookRepository extends JpaRepository<BookEntity, Integer>{
	List<BookEntity> findByNameContaining(String name);
}
