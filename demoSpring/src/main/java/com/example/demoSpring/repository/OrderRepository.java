package com.example.demoSpring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demoSpring.entity.OrderEntity;
@Repository
public interface OrderRepository extends CrudRepository<OrderEntity,Integer>{

}
